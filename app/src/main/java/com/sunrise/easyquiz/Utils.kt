package com.sunrise.easyquiz

object Constants {

    const val USER_NAME: String = "user_name"
    const val TOTAL_QUESTIONS: String = "total_question"
    const val CORRECT_ANSWER: String = "correct_answers"


    fun getQuestion(): ArrayList<Question> {
        val questionList = ArrayList<Question>()

        val que1 =
            Question(
                1,
                R.drawable.question_one,
                "1",
                "2",
                "12",
                "10",
                3
            )

        val que2 =
            Question(
                2,
                R.drawable.question_two,
                "Лошади",
                "Зайцы",
                "Рыбы",
                "Воробьи",
                3
            )

        val que3 =
            Question(
                3,
                R.drawable.question_three,
                "15",
                "0",
                "30",
                "45",
                1
            )
        val que4 =
            Question(
                4,
                R.drawable.question_four,
                "Кошки",
                "Раки",
                "Куры",
                "Коровы",
                2
            )
        val que5 =
            Question(
                5,
                R.drawable.question_five,
                "Песочные",
                "Сломанные",
                "Биг Бен",
                "Солнечные",
                2
            )
        val que6 =
            Question(
                6,
                R.drawable.question_six,
                "Сознание",
                "Сон",
                "Вес",
                "Память",
                3
            )
        val que7 =
            Question(
                7,
                R.drawable.question_seven,
                "Часы",
                "Противогаз",
                "Термометр",
                "Телефон",
                1
            )
        val que8 =
            Question(
                8,
                R.drawable.question_eight,
                "2",
                "8",
                "6",
                "4",
                2
            )
        val que9 =
            Question(
                9,
                R.drawable.question_nine,
                "Мост",
                "Украшение",
                "Водопад",
                "Животные",
                1
            )
        val que10 =
            Question(
                10,
                R.drawable.question_ten,
                "Машина",
                "Корабль",
                "Поезд",
                "Часы",
                4
            )

        questionList.add(que1)
        questionList.add(que2)
        questionList.add(que3)
        questionList.add(que4)
        questionList.add(que5)
        questionList.add(que6)
        questionList.add(que7)
        questionList.add(que8)
        questionList.add(que9)
        questionList.add(que10)
        return questionList
    }

}